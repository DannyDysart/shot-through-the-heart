﻿Code Documentation
Team Coding Project
Game Coding with Unity and C#, II

Project: Shot through the Heart
Team: Team Soup
Author: Kevin Caton-Largent

Class: GameState
Description: Monitors the active state of the game and terminates the game upon one of the end state triggers.

Properties:
public 	int playerHP
		int score
		PlayerController pc
		static int numTargets
		float timeTillEnd
		float gameDuration
		SceneLoader gameEnd
		Text countdownText
		Image countdownImg
		Text targetsText
		Image hpImg
		Text hpText
		
private float secondToMin
		int hpMax


Functions:

private void Start()
		void Update()
public 	void CheckWinConditions()
		void UpdateTimeDisplay(int min, int sec)

Testing Results:

[4-24-18 KCCL] Unit Testing - Start() properly initializes variables if inspector hasn't.
[4-24-18 KCCL] Unit Testing - Update() properly  calls CheckWinConditions & UpdateTimeDisplay and decrements the counter.
[4-24-18 KCCL] Unit Testing - CheckWinConditions() properly checked game state and loaded appropriate end scene.
[4-24-18 KCCL] Unit Testing - UpdateTimeDisplay() properly updates the clock display.


Bug Results:

[4-24-18 KCCL] Bug 1 - GameData was null. 
[4-24-18 KCCL] Fixed Bug 1 - GameData had to be created before the first game scene.
[4-24-18 KCCL] Bug 2 - timeTillEnd was not mutiplying properly. 
[4-24-18 KCCL] Fixed Bug 2 - had to reload the unity editor.
[4-24-18 KCCL] Bug 3 - UpdateTimeDisplay() didn't properly update minutes at the half minute interval. 
[4-24-18 KCCL] Fixed Bug 3 - Had to make it ceilToInt instead of roundToInt to add a ceiling and subtract minutes by 1.
[4-24-18 KCCL] Bug 4 - UpdateTimeDisplay() seconds didn't properly update at beginning and end. 
[4-24-18 KCCL] Fixed Bug 4 - Had to subtract by 1 to make sure the count was properly displayed.
[4-24-18 KCCL] Bug 5 - UpdateTimeDisplay() didn't properly display time when seconds/minutes was < 10. 
[4-24-18 KCCL] Fixed Bug 5 - Had to add conditions to update the display properly when seconds and/or minutes < 10.




Code DocumentationTeam Coding ProjectGame Coding with Unity and C#II

Project: Shot through the heart
tTeam: Team Soup
Author: Saud Alshammari

Class: chooseScene: 
This tool is for giving the player option to choose which level
they want to play.

Properties:

public	string	level1
public	string	level2
public	string	level3

Functions:

  void Update()
    
         if (hit.transform.tag == ("Level1"))

	SceneManager.LoadScene(level1);

if (hit.transform.tag == ("Level2"))

	SceneManager.LoadScene(level2);

if (hit.transform.tag == ("Level3"))

	SceneManager.LoadScene(level3);

Testing Results:
[5/7/2018] It didn't load the scenes. change it fromSceneManager.LoadScene("level1")
to SceneManager.LoadScene(level1), pass

Bug Results:
[5/14/2018] add the scenes to the build 



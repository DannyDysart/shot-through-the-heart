﻿Code Documentation
Team Coding Project
Game Coding with Unity and C#, I

Project: Sample Project
Team: Team Alpha
Author: Student X

Class: MyScriptName
Description: This tool takes a number and game object reference and does stuff after finding an average number.

Properties:

int 		myNumber
GameObject 	thisGO

Functions:

public void 	DoStuff()
	float 	FindMiddle( int X, int Y )

Testing Results:

[1-7-17 gs] Unit Testing - FindMiddle() returned average, pass.

Bug Results:

[1-14-17 gs] Bug - Null ref excep in DoStuff(). [1-14-17 gs] Fixed - Validate myNumber in Start().

﻿/// <summary>
/// Author: Nate Hales - Gun script, Contains the functions and data templates to use for gun shooting, aninmation, and Reloading functionality.
/// </summary>
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;


public class GunScript : MonoBehaviour {


	public GunData m_gunData;
    public ScopeGUI _Scope;


    [SerializeField] Animator _gunAnimator;
    [SerializeField] Vector3 laser_OffeSet;

	private float timepiece;
    private Vector3 o_Position;
	private bool bCanShoot = true, bIsReloading = false;

    private bool bIsAming = false;

    void Start(){
		//Load first clip for the player
		m_gunData.ammoCount = m_gunData.clipNum;
	}


	// Update is called once per frame
	void Update () {

		if (timepiece> 0f)
		{
			timepiece -= Time.deltaTime;
		}

		else
		{
			bCanShoot = true; 
			bIsReloading = false;
		}
       
    }

    void OnGUI()
    {
        if (m_gunData.ammoCount <= 0)
        {
            Rect calcRect = new Rect();
            calcRect.height = Screen.height * .25f;
            calcRect.width = Screen.width * .25f;
            calcRect.x = Screen.width * .5f;
            calcRect.y = Screen.height * .35f;

            GUI.Label(calcRect, "Reload (R)");
        }

    }


    public void tryToFire()
	{
		print ("Bang");
		//if it still has ammo in the current clip
		if (m_gunData.ammoCount > 0)
		{
		    //Use a bullet to fire
		    m_gunData.ammoCount--;

            //set the timer to the firing time
            //timepiece = m_gunData.firingAnim.length;

            //set the timer to the fire rate
            timepiece = m_gunData.fireRate;

			//Racast logic for the bullet
			RaycastHit hit = new RaycastHit ();
			
			//Box cast for shoutgun firing
			if (m_gunData.m_weaponType == GunData.weaponType.shotGun)
			{
				
				if (Physics.BoxCast(gameObject.transform.position, transform.localScale ,gameObject.transform.forward, out hit, transform.rotation, m_gunData.MaxTravelDistance))
				{
					if (hit.collider.GetComponent<targetmove>() != null)
					{
                        hit.collider.gameObject.SetActive(false);
                        print("Hit: " + hit.collider.name);
                        GameData.current.targetsHit++;

                    }
                }
			}
            if (m_gunData.m_weaponType == GunData.weaponType.pistol)
            {
                //Ray cast for Pistol firing
                if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, m_gunData.MaxTravelDistance))
                {
                    if (hit.collider.CompareTag("Target"))
                    {
                        // Debug.DrawRay(,);
                        hit.collider.gameObject.SetActive(false);
                        print("Hit: " + hit.collider.name);
                        GameData.current.targetsHit++;
                    }
                }
            }
            if (m_gunData.m_weaponType == GunData.weaponType.sniper)
            {
                //Ray cast for sniper firing
                if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit))
                {
                    if (hit.collider.CompareTag("Target"))
                    {
                        // Debug.DrawRay(,);
                        hit.collider.gameObject.SetActive(false);
                        print("Hit: " + hit.collider.name);
                        GameData.current.targetsHit++;

                    }
                }
            }
            bCanShoot = false;

            //TODO: play animation and audio
        }
		else
		{

			print("Can't shoot right now");
			return;
		}
		
	}

	public void Reload()
	{
		if (bIsReloading == false)
		{
			bIsReloading = true;
			//timepiece = m_gunData.reloadAnim.length;
			m_gunData.ammoCount = m_gunData.clipNum;

			//Send to be played
			//m_gunData.reloadSound;
		}
	}

    public void AimDownSights()
    {

        if (m_gunData.m_weaponType == GunData.weaponType.sniper)
        {
            _Scope.enabled = !_Scope.enabled;
        }
        else
        {
            print("THis gun doesn't have a scope!");
        }
        if (_Scope.enabled == false) { gameObject.transform.position = o_Position; } else
        {
            o_Position = gameObject.transform.position;
            gameObject.transform.position = Camera.main.transform.position;
        }
    }

    void OnDisable()
    {
        _Scope.enabled = false;
        gameObject.transform.position = o_Position; 
    }


}

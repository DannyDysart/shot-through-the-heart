﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScopeGUI : MonoBehaviour {

	[SerializeField] Rect scopeRect;
	[SerializeField] Texture scopeTexture;
    [Header("The smailler the number the stronger the magnification when zoomed in.")]
    [SerializeField] private float _ZoomedFOV; 

    private float o_Fieldofview;

	// Use this for initialization
	void OnEnable () {

        //get the original field of view setting
	    o_Fieldofview = Camera.main.fieldOfView;

        //Set camera's field of view(Zoom in)
        Camera.main.fieldOfView = _ZoomedFOV;
    }

    void OnDisable(){
        //Set camera's field of view(Zoom out)
        Camera.main.fieldOfView = o_Fieldofview;
    }

    // Update is called once per frame
	void OnGUI () {
		//place holder rec for calculating the proper propotions
		Rect calcRect = new Rect();

		calcRect.x = scopeRect.x * Screen.width;//X calculation
		calcRect.y = scopeRect.y * Screen.height;//Y calculation
		calcRect.height = scopeRect.height * Screen.height;//height calculation
		calcRect.width = scopeRect.width * Screen.width;//width calculation

		GUI.DrawTexture (calcRect,scopeTexture);
	}
}

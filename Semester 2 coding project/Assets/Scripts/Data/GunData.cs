﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Nate Hales - Gun data. Basic class template for Sotring data relevant to a guns stats.
/// </summary>

[System.Serializable]
public class GunData {
	public string gunName;

	public int gunIndex;
	public int clipNum; // how many bullets each reload gives
	public int ammoCount; // current ammount of shoots left in the present clip.

	public float blowBackAmount;
	public float bulletDamage;
	public float accurancy;
	public float MaxTravelDistance;
	public float fireRate;

	public enum weaponType{
		pistol,
		shotGun,
		sniper
	};

	public enum engagementRange{
		close,
		medium,
		far
	};

	public AudioClip reloadSound, fireSound;

	public AnimationClip reloadAnim, firingAnim;

	public engagementRange m_EngagementRange;

	public weaponType m_weaponType;

}

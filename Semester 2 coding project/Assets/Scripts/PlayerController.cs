﻿///Author: Nate Hales
///Editor: Kevin Caton-Largent (KCCL)
/// Player movement, aiming functionallity, with limited references to calling shooting reloading, and ADS for to the players currently equpied gun if vaild.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float movementSpeed, jumpPower;
	public float _Sensitivity, rotationSensitivity;
    public int hp; // KCCL : for health tracking
   
    public Transform gunHand;

    private GunScript m_gun;
	Rigidbody _rigi;

    void Awake()
    {
        //Lock/Hide the cursor to the center of the game view
        Cursor.lockState = CursorLockMode.Locked;

    }


    // Use this for initialization
	void Start () {
        // KCCL : if HP hasn't been assigned in the inspector give it default 20
        if (hp <= 0) 
            hp = 20;
        // KCCL : End of edit

		_rigi = GetComponent<Rigidbody> ();
	}

 
    void FixedUpdate () {
		
		Move();
        Aim();

        //Jump Fuction
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("Jump");
            _rigi.AddForce(0f, (jumpPower*100) * Time.deltaTime, 0f, ForceMode.Impulse);
        }

        //Only if you have a gun
        if (m_gun != null)
	    {
	        //ZoomIn
	        if (Input.GetButtonDown("Fire2"))
	        {
	            m_gun.AimDownSights();
	        }

	        //Shoot
	        if (Input.GetButtonDown("Fire1"))
	        {
	            m_gun.tryToFire();
	        }

	        //Reload
	        if (Input.GetKeyDown(KeyCode.R))
	        {
	           m_gun.Reload();
	        }
	    }

	}

	void Move ()
	{
		Vector3 newMove = gameObject.transform.position;
		//move forward 
		if(Input.GetAxis("Vertical")>_Sensitivity){
			newMove += gameObject.transform.forward * movementSpeed * Time.deltaTime;
		}
		//move backward
		if(Input.GetAxis("Vertical")< -_Sensitivity){
			newMove += -gameObject.transform.forward * movementSpeed * Time.deltaTime;
		}
		//Move Right
		if(Input.GetAxis("Horizontal")>_Sensitivity){
			newMove += gameObject.transform.right * movementSpeed * Time.deltaTime;
		}
		//Move left
		if(Input.GetAxis("Horizontal")<-_Sensitivity){
			newMove += -gameObject.transform.right * movementSpeed * Time.deltaTime;
		}
		gameObject.transform.position = newMove;
	}

    void Aim()
    {
        // Store the mouses current x and y positions
        Vector2 mousePos = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")); 
        
        //Multiply our mouse positions by the Rotsenitivy to adjust the rate of rotation
        Vector2 newRot = new Vector2(mousePos.x * rotationSensitivity, mousePos.y * rotationSensitivity);

        //store our gameobjects current rotations in euler for calculations later.
        Vector3 _TargetRotation = transform.rotation.eulerAngles;

        //Assign/calculate our new target rotation.
        _TargetRotation.x -= newRot.y;
        _TargetRotation.y += newRot.x;

        //Set the game objects rotation (convert the euler angle back into a quaternion)
        transform.rotation = Quaternion.Euler(_TargetRotation);

    }


    public void SetupNewGun(GunScript GunPickUP)
    {
        m_gun = GunPickUP;
    }

}

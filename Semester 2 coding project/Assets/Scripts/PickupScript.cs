﻿///Author: Nate hales
/// simple script for assigning a gun to the players hand with the collid with the pick up object
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour {


	[SerializeField] GameObject weaponPickup;

	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag == "Player") {
			if(weaponPickup != null){giveWeapon (col.gameObject);}
			gameObject.SetActive(false);// turn of object
		}
	}


	void giveWeapon(GameObject player){
		PlayerController plCtrl = player.GetComponent<PlayerController> ();

	   //create and make new gun a child of the player.
        GameObject newGun = Instantiate (weaponPickup, plCtrl.gunHand.position, plCtrl.gunHand.rotation, plCtrl.gunHand) as GameObject;

		plCtrl.SetupNewGun(newGun.GetComponent<GunScript>());//Signal player controller to set up the new weapon.
	}

}

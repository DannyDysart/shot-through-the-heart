﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;
/// <summary>
/// Author: Kevin Caton-Largent
/// Serialize the current game data information and implement loading
/// </summary>
public class SaveLoadState {
	public static GameData gameData = GameData.current;
    public static void SaveGameState()
    {
        if (!File.Exists(Application.persistentDataPath + "/data.gd"))
        {
            // create a binary formatter to prep data serialization
            BinaryFormatter bf = new BinaryFormatter();
            // create a file named data.gd
            FileStream file = File.Create(Application.persistentDataPath + "/data.gd");
            // serialize the data from GameData to the file
            bf.Serialize(file, GameData.current);
            // close the file
            file.Close();
        }
        else
        {
            // create a binary formatter to prep data serialization
            BinaryFormatter bf = new BinaryFormatter();
            // open a file named data.gd
            FileStream file = File.Open(Application.persistentDataPath + "/data.gd", FileMode.Open);
            // serialize the data from GameData.current to the file
            bf.Serialize(file, GameData.current);
            // close the file we no longer need its data
            file.Close();
        }
        
    }
	public static void LoadGameState()
	{
		// check if the file exists and then load the file
		if (File.Exists(Application.persistentDataPath + "/data.gd"))
		{
            // create a binary formatter to prep data deserialization
            BinaryFormatter bf = new BinaryFormatter();
            // open the pre-existing save file that contains the serialized data
			FileStream file = File.Open(Application.persistentDataPath + "/data.gd", FileMode.Open);
            // deserialize the data into the gameData class
            // now gameData contains the appropriate information about the current game state
			gameData = (GameData)bf.Deserialize(file);
            // close the file we no longer need its data
			file.Close();
		}
	}
    public static void ResetGameState()
    {
        GameData.current = new GameData();
    }
   
}

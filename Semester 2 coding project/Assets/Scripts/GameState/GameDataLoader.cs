﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Kevin Caton-Largent
/// Loads the GameData and populates the save data information in the end scene.
/// </summary>
public class GameDataLoader : MonoBehaviour {
    // text that contains the winOrLose to update, and the scoreText
    public TextMesh winOrLose, scoreText, subTitle; 
    void Start () {
      
        if (winOrLose != GameObject.Find("TEXT").transform.Find("End Text").GetComponent<TextMesh>())
            winOrLose = GameObject.Find("TEXT").transform.Find("End Text").GetComponent<TextMesh>();
        if (scoreText != GameObject.Find("TEXT").transform.Find("Score Text").GetComponent<TextMesh>())
            scoreText = GameObject.Find("TEXT").transform.Find("Score Text").GetComponent<TextMesh>();
        if (subTitle != GameObject.Find("TEXT").transform.Find("Sub-Title").GetComponent<TextMesh>())
            subTitle = GameObject.Find("TEXT").transform.Find("Sub-Title").GetComponent<TextMesh>();
        SaveLoadState.LoadGameState();
        if (GameData.current != null)
        {
            winOrLose.text = GameData.current.endText;
            scoreText.text += "" + GameData.current.targetsHit;
            if (GameData.current.winOrLoss)
            {
                winOrLose.color = Color.green;
                subTitle.text = "(Try Again?)";
            }
        }
        else
        {
            Debug.LogError("GameData was null in GameDataLoader script. Exiting run.");
            UnityEditor.EditorApplication.isPlaying = false;
        }

    }
	
	
}

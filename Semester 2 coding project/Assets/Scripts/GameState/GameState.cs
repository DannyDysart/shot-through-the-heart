﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// Author: Kevin Caton-Largent
/// Monitors the active state of the game and terminates the game upon one of the end state triggers.
/// </summary>
public class GameState : MonoBehaviour {

	// used for updating targetsDisplay
	public int targetsLeft;
    public int score;

    // player health
    public int playerHP;

    // player controller to reference health
    PlayerController pc;

    // total number of targets
    public static int numTargets = 0;
    
    // time till the game ends in minutes
    public float timeTillEnd;
	// total duration of game
	public float gameDuration;
	// seconds to minute conversion
	private float secondToMin = 60.0f;


    // Countdown timer text and image
    public Text countdownText;
	public Image countdownImg;

    // Targets text
    public Text targetsText;
    // HP image 
    Image hpImg;

    // helps update the fill image properly for hp
    private int hpMax;

    void Start()
    {
        Time.timeScale = 1;
        if (pc == null || pc != GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>())
            pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        playerHP = pc.hp;
        hpMax = playerHP;

        if (countdownImg == null || countdownImg != GameObject.Find("UI").transform.Find("Countdown BG").transform.Find("Countdown IMG").GetComponent<Image>())
			countdownImg = GameObject.Find("UI").transform.Find("Countdown BG").transform.Find("Countdown IMG").GetComponent<Image>();

		if (countdownText == null || countdownText != GameObject.Find("UI").transform.Find("Countdown BG").transform.Find("Countdown Text").GetComponent<Text>())
			countdownText = GameObject.Find("UI").transform.Find("Countdown BG").transform.Find("Countdown Text").GetComponent<Text>();

        if (targetsText == null || targetsText != GameObject.Find("UI").transform.Find("Targets BG").transform.Find("Targets Text").GetComponent<Text>())
            targetsText = GameObject.Find("UI").transform.Find("Targets BG").transform.Find("Targets Text").GetComponent<Text>();

        if (hpImg == null || hpImg != GameObject.Find("UI").transform.Find("HP BG").transform.Find("HP IMG").GetComponent<Image>())
            hpImg = GameObject.Find("UI").transform.Find("HP BG").transform.Find("HP IMG").GetComponent<Image>();

        if (GameData.current == null)
            SaveLoadState.ResetGameState();

        targetsText.text = "Targets " + numTargets;
        targetsLeft = numTargets;
        score = 0;
        hpImg.fillAmount = playerHP / hpMax;
		// start out with one minute if the inspector hasn't set the timer
		if (timeTillEnd <= 0)
			timeTillEnd = 1.0f;
		timeTillEnd *= secondToMin;
		gameDuration = timeTillEnd;
	}
    /// <summary>
    /// Checks the active state of the game for any indicators like all targets destroyed or hp gone
    /// then updates correspondingly to ensure that the proper screen is displayed on game end
    /// </summary>
	public void CheckWinConditions()
	{
		if (GameData.current != null)
		{
            // if player has hit all the targets load the final scene
            if (GameData.current.targetsHit >= numTargets)
            {
                // update the win or loss boolean
                GameData.current.winOrLoss = true;
                // update the end text
                GameData.current.endText = "YOU WIN!";

                // serialize GameData into a file so that it can be loaded at the game end scene
                SaveLoadState.SaveGameState();
                // load the game end scene
                SceneManager.LoadScene("death scene");
            }

            if (playerHP <= 0f)
			{
                GameData.current.endText = "Wait u died?";
                GameData.current.winOrLoss = false;
				// serialize GameData into a file so that it can be loaded at the game end scene
				SaveLoadState.SaveGameState();
                // load the game end scene
                SceneManager.LoadScene("death scene");
			}
			else if (timeTillEnd <= 0f)
			{
				// if the ammount of targetsHit is 0, player didn't score anything and has lost
				if (GameData.current.targetsHit == 0)
				{
                    // update the win or loss boolean
                    GameData.current.winOrLoss = false;
                    // update the end text
                    GameData.current.endText = "YOU RAN OUT OF TIME!";
				}                
                // player scored but ran out of time
                else
                {
                    // update the win or loss boolean
                    GameData.current.winOrLoss = false;
                    // update the end text
                    GameData.current.endText = "YOU RAN OUT OF TIME!";
                }
				// serialize GameData into a file so that it can be loaded at the game end scene
				SaveLoadState.SaveGameState();
				// load the game end scene
	
				SceneManager.LoadScene("death scene");
			}


		}
		else
		{
			Debug.LogError("Game Data wasn't instantiated in the GameState script @" + SceneManager.GetActiveScene().name + ". Exiting run.");
			UnityEditor.EditorApplication.isPlaying = false;
		}

	}
    /// <summary>
    /// Updates the clock to represent the correct amount of time left
    /// </summary>
    /// <param name="min">total minutes left</param>
    /// <param name="sec">total seconds left</param>
	public void UpdateTimeDisplay(int min, int sec)
	{
		//print ("Test" + countdownImg.fillAmount);
		if (min < 1) {
            if (sec >= 10)
                countdownText.text = "0" + min + ":" + sec;
            else
                countdownText.text = "0" + min + ":0" + sec;
		} else {
			if (min >= 10) {
				if (sec >= 10)
					countdownText.text = min + ":" + sec;
				else
					countdownText.text = min + ":0" + sec;
			} else {
				if (sec >= 10)
					countdownText.text = "0" + min + ":" + sec;
				else
					countdownText.text = "0" + min + ":0" + sec;
			}
           
           
		}
        countdownImg.fillAmount = timeTillEnd / gameDuration;

    }
  

    void Update()
	{
		// pretesting keystrokes to test if the checkWinConditions are working
		if (Input.GetKeyDown (KeyCode.T))
			GameData.current.targetsHit++;
		else if (Input.GetKeyDown (KeyCode.H))
			pc.hp--;

        if (score != GameData.current.targetsHit)
        {
            score = GameData.current.targetsHit;
            targetsLeft = numTargets - score;
            if (targetsLeft <= 1)
                targetsText.text = "Target " + targetsLeft;
            else
                targetsText.text = "Targets " + targetsLeft;
        }

        // updates local playerHP only if the external HP has been modified
        if (playerHP != pc.hp)
            playerHP = pc.hp;
      
		// countdown until time is out
		timeTillEnd -= Time.deltaTime;
		
		int secondsDisplay = Mathf.RoundToInt ((timeTillEnd) % secondToMin);
		int minutesDisplay = Mathf.CeilToInt(timeTillEnd / secondToMin);
		UpdateTimeDisplay (minutesDisplay-1, secondsDisplay-1);

        hpImg.fillAmount = (float)playerHP /  (float)hpMax;
		CheckWinConditions();
	}
}

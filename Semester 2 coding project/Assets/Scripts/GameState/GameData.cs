﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
// Author : Kevin Caton-Largent
// A serializable Class that contains the game data and game status for load in another scene
public class GameData {
	public static GameData current;
    public static int totalTargets;
	public int targetsHit;
	public bool winOrLoss;
	public string endText;
	public GameData()
	{
        totalTargets = 0;
		targetsHit = 0;
		winOrLoss = false;
		endText = "";
	}
}

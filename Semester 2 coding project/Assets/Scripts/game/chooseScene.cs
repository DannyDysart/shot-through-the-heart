﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class chooseScene : MonoBehaviour {
/*
 * Author Danny Dysart & Saud Alshammari
 * Editor : Kevin Caton-Largent (KCCL)
 * Coding 2 solo Project
 * With Assistence from Gelnn storm and Joseph Koroma Thx again to them !
 */ 
	// Splat paint on the wall ie shoot the thing
	// detect that player pressed fire 
	//from the veiw of the camera, cast a ray and some information 
	// create a copy of splat prefab 
	//place that prefab at the point o the wall 
	// rotate copy to be flat against the wall 
	// offset the copy away formm the wall 
	// load the scene chosen

	public int numberTargets;
	public GameObject splatPrefab;
	public	string	level1;
	public	string	level2;
	public	string	level3;

    private GameObject gunObj; // KCCL : want the gun to disapear when picked up, and want to only be able to fire when it is picked up
    private bool gunPickedUP;

	void Start () {
        // KCCL : does gun game object exist? if so assign it to the gunObj otherwise exit application
        if (GameObject.Find("SM_Wep_Rifle_01"))
            gunObj = GameObject.Find("SM_Wep_Rifle_01");
        else
        {
            if (Application.isEditor)
            {
                Debug.LogError("The gun doesn't exist in level01 exiting run");
                UnityEditor.EditorApplication.isPlaying = false;
            }
            else
            {
                Debug.LogError("The gun doesn't exist in level01 exiting application");
                Application.Quit();
            }
        }
        // KCCL : End of edit
	}

    // Update is called once per frame
    void Update()
    // detect that player pressed fire 
    {
        if (gunPickedUP) // KCCL : need to check if the gun has been picked up which is set by the collision to true if hit
        {
            if (Input.GetMouseButtonDown(0))
            {
                //from the veiw of the camera, cast a ray and some information 


                RaycastHit hit = new RaycastHit();
                if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit))
                {
                    // create a copy of splat prefab 
                    GameObject go = GameObject.Instantiate(splatPrefab);
                    go.name = "Splat";
                    Vector3 wallPoint = hit.point;
                    Quaternion wallOrientaion = Quaternion.LookRotation(hit.normal);
                    // offset the copy away formm the wall 
                    go.transform.position = wallPoint;
                    wallPoint += (hit.normal * 0.001f);

                    //place that prefab at the point o the wall 
                    go.transform.position = wallPoint;
                    // rotate copy to be flat against the wall 
                    go.transform.rotation = wallOrientaion;
                    //what the ray cast hits target 
                    if (hit.transform.tag == ("Level1"))
                    {
                        numberTargets -= 1;
                        //makeing shure that you cant keep shooting the same target over and over 
                        hit.transform.GetComponent<Collider>().enabled = false;
                        //removes target tag from object 
                        hit.transform.tag = null;
                        //run this function, destory the game object after 2 sec.
                        Destroy(hit.transform.gameObject, 2);
                        // Load the scene
                        SceneManager.LoadScene(level1);
                    }

                    if (hit.transform.tag == ("Level2"))
                    {
                        numberTargets -= 1;
                        //makeing shure that you cant keep shooting the same target over and over 
                        hit.transform.GetComponent<Collider>().enabled = false;
                        //removes target tag from object 
                        hit.transform.tag = null;
                        //run this function, destory the game object after 2 sec.
                        Destroy(hit.transform.gameObject, 2);
                        //Load the scene
                        SceneManager.LoadScene(level2);
                    }

                    if (hit.transform.tag == ("Level3"))
                    {
                        numberTargets -= 1;
                        //makeing shure that you cant keep shooting the same target over and over 
                        hit.transform.GetComponent<Collider>().enabled = false;
                        //removes target tag from object 
                        hit.transform.tag = null;
                        //run this function, destory the game object after 2 sec.
                        Destroy(hit.transform.gameObject, 2);
                        // Load the scene
                        SceneManager.LoadScene(level3);

                    }

                }






            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == gunObj)
        {
            gunPickedUP = true;
            other.gameObject.SetActive(false);
        }
    }
}

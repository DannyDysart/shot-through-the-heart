﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*Author Danny Dysart 
 * Codeing2 solo Project
 * With Assistence from Kevin and Alec and Joseph Koroma Thx again to them !
 */ 

public class itempickup : MonoBehaviour {
	public GameObject gun;
	bool hit;
	public GameObject tips;

	// Use this for initialization
	void Start () 
	{
		hit = false;
	}
	
	void Update(){
		//if u hit the f key 
		if (Input.GetKeyDown (KeyCode.F) && hit) 
		{
			//this game oject dissapears 
			this.gameObject.SetActive (false);
			//set the gun object to on, ie in oyur hands now
				gun.SetActive(true);
			tips.SetActive (false);
		}

		if(Input.GetKeyDown(KeyCode.A))
			{
			if (tips.activeSelf)
				tips.SetActive (false);
			else
				tips.SetActive (true);
			}
	}
	
	//when u enter the trigger
	void OnTriggerEnter(Collider other)
	{
		//print this 
		print ("entered thing  ");
		//find the object with the tag player, turn hit on
		if (other.gameObject.CompareTag("Player"))
		{
			hit = true;
		}
		tips.SetActive (true);


	}
	//when u exit turn hit off
	void OnTriggerExit (Collider other)
	{
		hit = false;
	}
}

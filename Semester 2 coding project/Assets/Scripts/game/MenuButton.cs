﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour {
/* Author Danny Dysart 
 * Editor : Kevin Caton-Largent (KCCL)
 * Coding2 Team Project
 * With Assistence from Gleen storm Thx again to him !
 */ 

 



	// This script manages the main menu button(s), particularly to launch the game

	public	Rect		gameStartButtonRect;
	public	GameObject	gameLoadTrigger;

	public	int 		fontSizeAt1024 = 24;

	public	TextMesh	titleObject;
	public	TextMesh	subTitleObject;

	public	Color 		titleFlashColor;
	public	Color		titleNormalColor;
	public	float 		titleFlashTime = 2f; // the amount of time to wait until changing color

	private	float 		timePassing; // use this as a timer, counting down

    void Start () {
        // KCCL : if the player has won change the flash to blue so its green and blue colors
        if (GameData.current != null)
        {
            if (GameData.current.winOrLoss)
                titleFlashColor = Color.blue;
        }
        // KCCL : End of Edit
            // validate the input
            // provide warnings or errors if there are problems with input

            if (titleFlashColor == titleNormalColor) {
			// this is a problem, but it can still run, so warn
			Debug.LogWarning( gameObject.name+" : Title Flash Color equals Title Normal Color. If they are different, the title wil appear to flash. Will ignore.");
		}
		if (titleFlashTime <= 0f) {
			Debug.LogError ( gameObject.name+" : Title Flash Time invalid. It should be set to a positive number. Aborting.");
			enabled = false;
		}

		// initialize the normal title color by reading the color that was set
		titleNormalColor = titleObject.color;
		// initialize the time passing to the maximum flash time
		timePassing = titleFlashTime;

        // KCCL : need to see cursor at end game
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        // KCCL : End of edit


    }


	
	void Update () {
		
		// use the passage of time to flash the title by toggling colors

		// count down the time passing until it is zero, then reset it
		timePassing -= Time.deltaTime;
		// detect that time is zero (or below zero)
		if (timePassing <= 0f) {
			// reset time to maximum
			timePassing = titleFlashTime;
			// change color of title to flash color, if color is normal
			if (titleObject.color == titleNormalColor)
				titleObject.color = titleFlashColor;
			else
				titleObject.color = titleNormalColor; // else reset color
		}
	}

	void OnGUI() {

		// read the set rectangle for the button before adjusting for screen pixels
		Rect r = gameStartButtonRect;
		// read the screen pixels (so the rect can be adjusted to any size)
		float width = Screen.width;
		float height = Screen.height;
		// adjust the rectanglge for the button by multiplying the values by screen pixel dimensions
		r.x *= width;
		r.y *= height;
		r.width *= width;
		r.height *= height;

		GUIStyle g = new GUIStyle (GUI.skin.button);
		g.fontSize = Mathf.RoundToInt ( fontSizeAt1024 * ( width/ 1024f ) );

		// display a button that will say "START"
		if (GUI.Button (r, "START", g)) {
            // KCCL : Reset game state so score is zero on subsequent tries
            SaveLoadState.ResetGameState();
            GameState.numTargets = 0;
            // KCCL : End of edit

			// if this START button is pressed load the game scene by activating the load scene tool object
			SceneManager.LoadScene ("level01");

			}
	}

		

		



}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour {

	/*Author Danny Dysart 
     * Editor: Kevin Caton-Largent
 * Codeing2 team Project
 * With Assistence from Kevin and Alec and Joseph Koroma Thx again to them !
 */ 
	// This script loads a scene after a delay
    // *Edited version just loads the scene based on the string name when LoadScene() is called externally
	public	string	sceneName;
	public	float	loadDelay;
    public  bool    delayActive;

	void Start () {
		// TODO: validate
	}

	void Update() {

		// waits the load delay time before loading  scene

		// reduces the load delay by the passing of time
        if (delayActive)
            loadDelay -= Time.deltaTime;
		// if load delay is zero or below  calls Load Scene custom function
		if (loadDelay <= 0f && delayActive) {
			LoadScene ();
		}
	}

	public void LoadScene() {
		
		// loads  scene
		SceneManager.LoadScene( sceneName );
	}
}

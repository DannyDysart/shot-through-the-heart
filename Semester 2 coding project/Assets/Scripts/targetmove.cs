﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Chris
// Editor: Kevin Caton-Largent (KCCL)
public class targetmove : MonoBehaviour {

	public float amountTtoMovex;
	public float speed;
	private float currentposx;
	private float currentposy;
	public int facing;


	// Use this for initialization
	void Start () {
		//define the facing and currentposx
		//object start postion
		currentposx = gameObject.transform.position.x;
		facing = 0;
        // KCCL : if the GameStateMachine is not null add up this target to the total number of targets on start
        if (GameObject.Find("GameStateMachine") != null)
        {
            GameState.numTargets++;
        }
        // KCCL : End of edit

    }
	
	// Update is called once per frame
	void Update () {
		//move to left
		if (facing == 0 && gameObject.transform.position.x < currentposx - amountTtoMovex) {
			facing = 1;
		}
		//move to right
		if (facing == 1 && gameObject.gameObject.transform.position.x > currentposx) {
			facing = 0;
		}
		//move speed and and moving
		if (facing == 0) {
			transform.Translate (-Vector2.right * speed * Time.deltaTime);
		} else if (facing == 1) {
			transform.Translate (Vector2.right * speed * Time.deltaTime);
		}
	}

		
	}



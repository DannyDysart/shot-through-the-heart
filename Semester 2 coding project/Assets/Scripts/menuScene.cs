﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuScene : MonoBehaviour {
	// Author: Saud Alshammari
    // Editor: Kevin Caton-Largent (KCCL)
	public	string	level01;
	public string quit;
    

    // Use this for initialization
    public void Start()
    {
        // KCCL : need to initialize current with data to use later.
        // Current is static so one should exist across the entire run.
        SaveLoadState.ResetGameState();
    }
    
    public void startmenu () {
		SceneManager.LoadScene (level01);
		}

	public void quitmenu (){
		print ("quit!!");
        // KCCL : Editor application to stop the editor application (works when the game is unbuilt) 
        // Application.quit only works when running the built game
        if (Application.isEditor)
            UnityEditor.EditorApplication.isPlaying = false;
        // KCCL : End of edit

		Application.Quit ();
	}
}